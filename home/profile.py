from django.shortcuts import render
from .models import Narasumber, Universitas, NarasumberBerita, Berita
from django.db import connection

# Create your views here.
response={}
def profile(request):
	print("punyatipaw")
	try:
		if (request.session['status'] == 'true'):
			username=request.session['username']
			narasumber = Narasumber.objects.raw('SELECT * FROM Narasumber WHERE username=%s;',[username])[0]
			universitas = narasumber.id_universitas

			status_narasumber = ''; 
			narasumber_mahasiswa = Narasumber.objects.raw('SELECT COUNT(*), id FROM NARASUMBER WHERE NARASUMBER.id IN (SELECT id FROM MAHASISWA WHERE MAHASISWA.id_narasumber =%s) GROUP BY NARASUMBER.id', [narasumber.id])
			narasumber_dosen = Narasumber.objects.raw('SELECT COUNT(*), id FROM NARASUMBER WHERE NARASUMBER.id IN (SELECT id FROM DOSEN WHERE DOSEN.id_narasumber =%s) GROUP BY NARASUMBER.id', [narasumber.id])
			narasumber_staf= Narasumber.objects.raw('SELECT COUNT(*), id FROM NARASUMBER WHERE NARASUMBER.id IN (SELECT id FROM STAF WHERE STAF.id_narasumber =%s) GROUP BY NARASUMBER.id', [narasumber.id])
			
			if (len(list(narasumber_mahasiswa))) != 0:
				status_narasumber = 'Mahasiswa'
			elif (len(list(narasumber_staf))) != 0:
				status_narasumber= 'Staf'
			elif (len(list(narasumber_dosen))) != 0:
				status_narasumber= 'Dosen'

			narasumber_berita = NarasumberBerita.objects.raw('SELECT * FROM narasumber_berita WHERE narasumber_berita.id_narasumber = %s', [narasumber.id])
			list_of_berita = []
			for url_s in narasumber_berita:
				list_of_berita.append(url_s.url_berita)

			list_of_berita_satu = []
			for berita_s in list_of_berita:
				berita = Berita.objects.raw('SELECT * FROM berita WHERE berita.url = %s', [berita_s.url])
				list_of_berita_satu.append(berita)

			list_berita = []
			for judul in list_of_berita_satu:
				list_berita.append(judul[0])

			print(len(list_of_berita_satu))
			response['narasumber'] = narasumber
			response['universitas'] = universitas
			response['status_narasumber'] = {'status_narasumber':status_narasumber}
			response['berita'] = {'berita':list_berita}

			html = 'profile.html'
			return render(request, html, response)

			'''
			universitas = narasumber.id_universitas
			narasumber_berita = NarasumberBerita.objects.filter(id_narasumber=narasumber)
			daftar_berita = Berita.objects.filter(url__in=narasumber_berita)

			response['narasumber'] = narasumber
			response['universitas'] = universitas
			response['daftar_berita'] = daftar_berita
			return render(request, html, response)'''

		else:
			return HttpResponseRedirect(reverse('home:index'))
	except Exception as e:
		print(e)
		print('hehe')
		return HttpResponseRedirect(reverse('home:index'))

	

def berita(request):
	judul = "SELECT judul from berita, NarasumberBerita, Narasumber where username = request.session.username and narasumber.id = narasumber_berita.id_narasumber and berita.url = narasumber_berita.url_berita"

	with connection.cursor() as cursor:
		cursor.execute(nama)