from django.conf.urls import url
from .landingpage import index
from .landingpage import returnRegister
from .landingpage import register
from .custom_auth import auth_login, auth_logout
from .profile import profile
from .form_berita import newArticle, createNewArticle, createNewPoll
from .form_berita import home

#url for app, add your URL Configuration

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
    url(r'^register/$', returnRegister, name='returnRegister'),
    url(r'^custom_auth/register/$', register, name='register'),
    url(r'^profile/$', profile, name='profile'),
    url(r'^new-article/$', newArticle, name='new-article'),
    url(r'^create-new-article/$', createNewArticle, name='create-new-article'),
    url(r'^crete-new-poll/$', createNewPoll, name='create-new-poll'),
    url(r'^form/$', home, name='form'),
]
